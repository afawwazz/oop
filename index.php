<?php
// spl_autoload_register(function ($class_name) {
    // include $class_name . '.php';
// });
require_once 'src/animal.php';
require_once 'src/Frog.php';
require_once 'src/Ape.php';

function info(Animal $animal){
    echo 'Name : ' . $animal->get_name();
    echo '<br>';
    echo 'legs : ' . $animal->get_legs();
    echo '<br>';
    echo 'cold blooded : ' . $animal->get_cold_blooded();
    echo '<br>';
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
}

$sheep = new Animal("shaun");
info($sheep);
echo '<br>';

$kodok = new Frog("buduk");
info($kodok);
echo 'Jump : ';
$kodok->jump(); // "hop hop"
echo '<br>';
echo '<br>';

$sungokong = new Ape("kera sakti");
info($sungokong);
echo 'Yell : ';
$sungokong->yell(); // "Auooo"
echo '<br>';
echo '<br>';
?>